<?php

namespace Yugasa\Greeting;

class Greeting
{
    public function greet(String $sName)
    {
        return 'Good Morning ' . $sName;
    }
}
