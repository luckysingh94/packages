<?php

use Illuminate\Support\Facades\Route;
use Yugasa\Greeting\Greeting;

Route::get('/greet/{name}', function($sName) {
    $greeting = new Greeting();
    return $greeting->greet($sName);
});
